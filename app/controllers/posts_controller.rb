class PostsController < ApplicationController
  before_action :check_signed_in, except: :show
  before_action :set_post, only: [:show, :destroy, :edit, :update]


  def show
    @post = Post.find(params[:id])
  end

  def new
    @post = Post.new
  end

  def create
    @post = current_user.posts.build post_params
    if @post.save
      redirect_to @post
    else
      render "new"
    end
  end

  def edit
    
  end
  
  def update
    if @post.update(article_params)
        flash[:notice] = "Successfully updated post!"
        redirect_to @post
      else
        flash[:alert] = "Error updating post!"
        render 'edit'
    end
  end
  
  private
  def post_params
    params.require(:post).permit(:image, :description)
  end

  def check_signed_in
    redirect_to signin_path unless signed_in?
  end
  
  def set_post
    @post = Post.find(params[:id])
  end

end
